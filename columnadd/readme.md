# Column Add
The objective of this exericse is to modify the current layout of the existing site by writing your own styling in the `style.css` file.

1. Edit the style of the site so that it becomes a two column layout with the image

2. Edit the style of the site so that it includes two additioanl sections with text in the left column and the images in the right (use the images that have not yet been added to the page, one for each section respectively.)

3. Add a new font to the project and use it to change the font of the headings. You can use fonts.google.com to pick a new font.

4. change the styling of the sections so that they are equal height, resizing the images to fit.

5. add a unique background colour to each section for contrast.

6. using the `main.js` file add a script that will scroll the page back to top.
    6.b. Write your own function to using a `loop` to make the scroll *smooth*

__tip__: You will need to use the `onclick event`, and the `window.scroll()` method.

**Example Layout**: You can change out the layout.png file for an example of what the finished layout might look like.

7. For the eager learner: Think of something you would like to build yourself, using what you've learnt.